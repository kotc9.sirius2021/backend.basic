﻿using Backend.Basic.Models;
using Microsoft.EntityFrameworkCore;
/* github/gitlab/telegram - @kotC9 */
namespace Backend.Basic.Database
{
    public class AppContext : DbContext
    {
        public DbSet<UrlData> Urls { get; set; }

        public AppContext()
        {
            Database.EnsureCreated();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Data Source=urls.db");
        }
    }
}