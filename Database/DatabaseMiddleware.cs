﻿using System;
using System.Linq;
using Backend.Basic.Models;
/* github/gitlab/telegram - @kotC9 */
namespace Backend.Basic.Database
{
    public class DatabaseMiddleware
    {
        public void Add(UrlData url)
        {
            using(var db = new AppContext())
            {
                Console.WriteLine($"Add: " +
                                  $"originalUrl: {url.OriginalUrl} " +
                                  $"shortenUrl: {url.ShortenedUrl}");

                db.Urls.Add(url);
                db.SaveChanges();
            }
        }

        public bool Exists(string shortenedUrl)
        {
            using(var db = new AppContext())
            {
                Console.WriteLine($"Exists: shortenUrl: {shortenedUrl}");

                var urlData = db.Urls.FirstOrDefault(url => url.ShortenedUrl == shortenedUrl);

                return urlData != null;
            }
        }

        public UrlData Find(string shortenedUrl)
        {
            using(var db = new AppContext())
            {
                Console.WriteLine($"Find: shortenUrl: {shortenedUrl}");

                UrlData urlData = null;

                foreach (var url in db.Urls)
                {
                    if (url.ShortenedUrl == shortenedUrl)
                    {
                        urlData = url;
                        break;
                    }
                }
                
                return urlData;
            }
        }

        public void Update(UrlData entity)
        {
            using(var db = new AppContext())
            {
                Console.WriteLine($"Update: shortenUrl: {entity.ShortenedUrl}");

                db.Urls.Update(entity);
                db.SaveChanges();
            }
        }

        public void AddView(string shortenedUrl)
        {
            using(var db = new AppContext())
            {
                var entity = db.Urls.FirstOrDefault(url => url.ShortenedUrl == shortenedUrl);
                // ReSharper disable once PossibleNullReferenceException
                entity.ViewsCount++;
                
                db.Urls.Update(entity);
                db.SaveChanges();
            }
        }
        
        public int? GetViews(string shortenedUrl)
        {
            var urlData = Find(shortenedUrl);

            return urlData?.ViewsCount;
        }
    }
}