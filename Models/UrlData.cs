﻿namespace Backend.Basic.Models
{
    public class UrlData
    {
        public int Id { get; set; }
        public string OriginalUrl { get; set; }
        public string ShortenedUrl { get; set; }
        public int ViewsCount { get; set; }
    }
}