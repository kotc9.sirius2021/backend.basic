﻿using System;
using Nancy.Hosting.Self;
/* github/gitlab/telegram - @kotC9 */
namespace Backend.Basic.Bootstrap
{
    public class Startup
    {
        static void Main(string[] args)
        {
            var bindUrl = new Uri(StartupConsts.Address);
            var bootstrapper = new Bootstrapper();
            var configuration = new HostConfiguration()
            {
                UrlReservations =
                {
                    CreateAutomatically = true
                }
            };
            
            using (var host = new NancyHost(bootstrapper, configuration, bindUrl))
            {
                host.Start();
                Console.WriteLine("NancyFX Stand alone test application.");
                Console.WriteLine("Press enter to exit the application");
                Console.WriteLine($"Started on {StartupConsts.Address}");
                Console.ReadLine();
            }
        }

    }
}