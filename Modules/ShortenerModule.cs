﻿using System;
using Backend.Basic.Bootstrap;
using Backend.Basic.Database;
using Backend.Basic.Extensions;
using Backend.Basic.Models;
using Nancy;
using Nancy.Hosting.Self;
using Nancy.ModelBinding;
using Nancy.Responses;
using HttpStatusCode = Nancy.HttpStatusCode;

/* github/gitlab/telegram - @kotC9 */

namespace Backend.Basic.Modules
{
    /// <summary>
    /// класс представляющий несколько роутов на get/post
    /// </summary>
    public class ShortenerModule : NancyModule
    {
        private readonly DatabaseMiddleware _databaseMiddleware;
        public ShortenerModule()
        {
            _databaseMiddleware = new DatabaseMiddleware();
            Post("/shorten", AddUrlDataAction);
            Get("/{shortenedUrl}", RedirectToOriginalAction);
            Get("/{shortenedUrl}/views", GetViewsAction);
            Get("/", HelloAction);
        }

        private object HelloAction(object arg)
        {
            return "Hello sirius2021";
        }

        private dynamic GetViewsAction(dynamic arg)
        {
            string shortenedUrl = arg.shortenedUrl;

            var views = _databaseMiddleware.GetViews(shortenedUrl);

            return views.HasValue
                ? Response.AsJson(new {viewCount = views.Value})
                : HttpStatusCode.NotFound;
        }

        private dynamic RedirectToOriginalAction(dynamic arg)
        {
            string shortenedUrl = arg.shortenedUrl;

            var urlData = _databaseMiddleware.Find(shortenedUrl);

            if (urlData != null)
            {
                _databaseMiddleware.AddView(shortenedUrl);
                return Response.AsRedirect(urlData.OriginalUrl, RedirectResponse.RedirectType.Permanent);
            }

            return HttpStatusCode.NotFound;
        }

        private dynamic AddUrlDataAction(dynamic arg)
        {
            var url = this.BindAnonymous(new {UrlToShorten = default(string)}).UrlToShorten;

            var data = new UrlData
            {
                OriginalUrl = url
            };
            
            if (!Uri.IsWellFormedUriString(url, UriKind.Absolute))
            {
                return HttpStatusCode.BadRequest;
            }
            
            do
            {
                data.ShortenedUrl = Nanoid.Nanoid.Generate(size: 8);
            } while (_databaseMiddleware.Exists(data.ShortenedUrl));


            _databaseMiddleware.Add(data);

            return Response.AsJson(new {status = "Created", shortenedUrl = data.ShortenedUrl}, HttpStatusCode.Created);
        }
    }
}