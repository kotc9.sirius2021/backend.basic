﻿using Nancy;
using Nancy.Extensions;
using Newtonsoft.Json;
/* github/gitlab/telegram - @kotC9 */
namespace Backend.Basic.Extensions
{
    public static class BindExtensions
    {
        public static T BindAnonymous<T>(this NancyModule m, T value)
        {
            return JsonConvert.DeserializeAnonymousType(m.Request.Body.AsString(), value);
        }
    }
}